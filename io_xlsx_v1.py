#!/usr/share/python3
from openpyxl import Workbook

name = input('Enter your name: ').strip()
age = input('How older are you? ').strip()

book = Workbook()
sheet = book.active

sheet['A1'] = 'Name'
sheet['B1'] = 'Age'

rows = {
    (name, age)
}

for row in rows:
    sheet.append(row)
    book.save('name.xlsx')
